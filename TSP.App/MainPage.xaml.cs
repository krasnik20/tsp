﻿using Microsoft.AspNetCore.Components.WebView.Maui;

namespace TSP.App;

public partial class MainPage : ContentPage
{
    public MainPage()
    {
        InitializeComponent();
        MainView.RootComponents.Add(new RootComponent { ComponentType = typeof(Client.Shared.App), Selector= "#app" });
    }
}