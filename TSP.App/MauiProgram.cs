﻿using TSP.Client.Shared.Shared;

namespace TSP.App;

public static class MauiProgram
{
    public static MauiApp CreateMauiApp()
    {
        var builder = MauiApp.CreateBuilder();
        builder
            .UseMauiApp<App>()
            .ConfigureFonts(fonts =>
            {
                fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
            });

        builder.Services.AddMauiBlazorWebView();
#if DEBUG
		builder.Services.AddBlazorWebViewDeveloperTools();
#endif

        builder.Services.AddSingleton<Localizations>();
        builder.Services.AddScoped(sp => new HttpClient 
        { 
            BaseAddress = new Uri("http://192.168.0.115:5221"),
            Timeout = TimeSpan.FromSeconds(30) 
        });

        return builder.Build();
    }
}