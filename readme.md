This project helps in applying Traveling Salesman Problem to real life cases.

### Purpose

My pet project for university (used it as graduate work for my bachelor degree).

### Functionality

This is the app, which can search addresses (using Bing Maps), and then find the shortest route to visit them all.

### Compatibility

It's the Blazor Hybrid App, thus can be run on Windows, Android, iOS, browser.

### Usage

Search for places by address, add them to list, then press "Find route", and you'll see the route: places in correct order and a map with build route.

