﻿function loadMap(locationRect) {
    window.map = new Microsoft.Maps.Map('#myMap', {
        bounds: locationRect,
        mapTypeId: Microsoft.Maps.MapTypeId.canvasLight,
        showDashboard: false,
        disableBirdseye: true
    });
}

function getLocationRect(bounds) {
    return Microsoft.Maps.LocationRect.fromEdges(bounds[0], bounds[1], bounds[2], bounds[3]);
}

window.LoadMap = (bounds, latitude, longitude) => {
    try {
        var locationRect = getLocationRect(bounds)

        if (!window.map) loadMap(locationRect);
        else window.map.setView({ bounds: locationRect });

        var pinLocation = new Microsoft.Maps.Location(latitude, longitude);
        var pin = new Microsoft.Maps.Pushpin(pinLocation);
        window.map.entities.pop();
        window.map.entities.push(pin);

        document.getElementById("myMap").scrollIntoView({ behavior: 'smooth', block: "center" });
    }
    catch (ex) {
        console.log(ex);
    }
}

window.ShowRoute = async (wayPoints) => {
    var promise = new Deferred();

    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', function () {
        try {

            if (!window.map) loadMap();

            var directionsManager = window.directionsManager = new Microsoft.Maps.Directions.DirectionsManager(window.map);
            directionsManager.setRequestOptions({ routeMode: Microsoft.Maps.Directions.RouteMode.driving });
            wayPoints
                .map(i => new Microsoft.Maps.Directions.Waypoint({ address: i.name, location: new Microsoft.Maps.Location(i.latitude, i.longitude) }))
                .map(i => directionsManager.addWaypoint(i));

            Microsoft.Maps.Events.addHandler(directionsManager, 'directionsUpdated', function (e) {
                promise.resolve();
            });

            directionsManager.calculateDirections();
        }
        catch (ex) {
            console.log(ex);
        }
    });

    await promise.promise;
    console.log("eyye");
}

window.ClearRoute = (dispose) => {
    window.directionsManager.clearAll();
    if (dispose) {
        window.directionsManager.dispose();
        window.directionsManager = undefined;
        window.map.dispose();
        window.map = undefined;
    }
}


function Deferred() {
    const p = this.promise = new Promise((resolve, reject) => {
        this.resolve = resolve;
        this.reject = reject;
    });
    this.then = p.then.bind(p);
    this.catch = p.catch.bind(p);
    if (p.finally) {
        this.finally = p.finally.bind(p);
    }
}