﻿namespace TSP.Client.Shared.Shared;

public class Localizations : Dictionary<Language, Localization>
{
    public Localizations()
    {
        Add(Language.English, 
            new Localization(
                "Search",
                "Type your request here...",
                "Start typing address",
                "Nothing was found",
                "Searching...",
                "Selected places",
                "Nothing was selected yet",
                "Find the route",
                "Select more places",
                "Clear",
                "Route",
                "Total length",
                "km.",
                "Route map is unavailable",
                "Route can't be shown on the map due to big amount of places",
                "Map will be shown here",
                "Click the address to see it on the map",
                "Sorry, you've already selected similar point",
                "Sorry, something went wrong",
                "Sorry, you've selected points unreachable from each other"));
        Add(Language.Russian, 
            new Localization(
                "Поиск мест",
                "Введите адрес...",
                "Начните вводить адрес",
                "Ничего не найдено",
                "Подождите, идет загрузка...",
                "Выбранные места",
                "Ничего не выбрано",
                "Найти маршрут",
                "Добавьте ещё адресов для поиска",
                "Очистить список",
                "Найденный маршрут",
                "Общая длина пути",
                "км.",
                "Карта маршрута недоступна",
                "Выбрано слишком большое количество точек маршрута для отображения",
                "Здесь будет карта",
                "Кликните на адрес, чтобы увидеть его на карте",
                "Точка с близкими координатами уже была добавлена",
                "Что-то пошло не так",
                "Вы выбрали точки, между которыми нет пути"
                ));
    }
}

public enum Language
{
    English, Russian
}

public record Localization(
    string SearchPlace,
    string SearchPlaceholder,
    string SearchNotStarted,
    string SearchNothingFound,
    string SearchInProgress,
    string SelectedPlaces,
    string SelectedPlacesListIsEmpty,
    string FindTheRoute,
    string InsufficientQuantityOfPlaces,
    string ClearListOfPlaces,
    string ResultRoute,
    string TotalLength,
    string Kilometers,
    string TooMuchPlacesToShowTitle,
    string TooMuchPlacesToShowExplanation,
    string NoMapYetTitle,
    string NoMapYetExplanation,
    string DuplicatedPoint,
    string SomethingWentWrong,
    string PointsAreUnreachable
    );