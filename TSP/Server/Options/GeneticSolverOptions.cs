﻿namespace TSP.Server.Options;

public class GeneticSolverOptions
{
    public int Generations { get; set; }
    public double MutationPossibility { get; set; }
    public int Population { get; set; }
}
