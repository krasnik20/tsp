﻿namespace TSP.Server.Options;

public class BingOptions
{
    public string ApiKey { get; set; }
    public string Url { get; set; }
}
