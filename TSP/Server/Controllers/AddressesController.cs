﻿using Mediator;
using TSP.Server.CQRS.Queries;
using TSP.Shared.WebInterfaces;

namespace TSP.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class AddressesController : IWebGeoService
{
    private readonly IMediator _mediator;

    public AddressesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet("SearchAddresses")]
    public async Task<ICollection<Address>> SearchAddresses(string searchKey) 
        => await _mediator.Send(new GetAddressesByNameQuery(searchKey));

    [HttpPost("FindRoute")]
    public async Task<RouteResult> FindRoute(ICollection<Address> addresses) 
        => await _mediator.Send(new GetRouteQuery(addresses));
}

