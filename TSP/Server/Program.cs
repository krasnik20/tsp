global using Microsoft.AspNetCore.Mvc;
global using TSP.Shared.Models;
using TSP.Server.Options;
using TSP.Server.Services.GeoServiceAdapters;
using TSP.Server.Services.GeoServiceAdapters.Bing;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

builder.Services.AddSingleton<IGeoServiceAdapter, BingAdapter>();
builder.Services.AddMediator();

builder.Services
    .AddOptions<BingOptions>()
    .Bind(builder.Configuration.GetSection(nameof(BingOptions)));

builder.Services
    .AddOptions<GeneticSolverOptions>()
    .Bind(builder.Configuration.GetSection(nameof(GeneticSolverOptions)));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}

//app.UseHttpsRedirection();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();

app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");

app.Run();
