﻿namespace TSP.Server.Services.GeoServiceAdapters;

public interface IGeoServiceAdapter
{
    Task<ICollection<Address>> SearchAddresses(string query, CancellationToken ct = default);
    Task<double[,]> GetDistances(ICollection<Address> addresses, CancellationToken ct = default);
}
