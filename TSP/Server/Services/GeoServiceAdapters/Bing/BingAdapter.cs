﻿using Microsoft.Extensions.Options;
using Refit;
using TSP.Server.Options;

namespace TSP.Server.Services.GeoServiceAdapters.Bing;

public class BingAdapter : IGeoServiceAdapter
{
    private readonly IBingClient _client;
    private string _bingMapsKey;

    public BingAdapter(IOptions<BingOptions> options)
    {
        var httpClient = new HttpClient() 
        { 
            BaseAddress = new Uri(options.Value.Url) 
        };

        _client = RestService.For<IBingClient>(httpClient);

        _bingMapsKey = options.Value.ApiKey;
    }

    public async Task<double[,]> GetDistances(ICollection<Address> addresses, CancellationToken ct = default)
    {
        int count = addresses.Count;

        var result = new double[count, count];

        var bingAddresses = addresses
            .Select(a => new BingCoords 
            { 
                Latitude = a.Latitude, 
                Longitude = a.Longitude 
            });

        var initialResponse = await _client.GetDistanceMatrix(
            _bingMapsKey, 
            new DistanceMatrixRequestBody
            {
                Destinations = bingAddresses,
                Origins = bingAddresses
            },
            ct: ct);

        var matrixInline = initialResponse.ResourceSets.First().Resources.First().Results;

        foreach (var cell in matrixInline)
            result[cell.OriginIndex, cell.DestinationIndex] = cell.TravelDistance;

        return result;
    }

    public async Task<ICollection<Address>> SearchAddresses(string query, CancellationToken ct = default)
    {
        var initialResponse = await _client.GetAddresses(query, _bingMapsKey, ct: ct);

        return initialResponse.ResourceSets
            .SelectMany(rs => rs.Resources)
            .Where(r => r.Confidence == "High" || r.Confidence == "Medium")
            .Select(r =>
            {
                var point = r.GeocodePoints.FirstOrDefault(c => c.UsageTypes.Contains("route")) ?? r.GeocodePoints.First();
                return new Address
                {
                    Latitude = point.Coordinates[0],
                    Longitude = point.Coordinates[1],
                    Name = r.Name,
                    Bounds = r.Bbox
                };
            })
            .ToList();
    }
}
