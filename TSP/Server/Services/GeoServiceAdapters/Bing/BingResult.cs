﻿namespace TSP.Server.Services.GeoServiceAdapters.Bing;

public class BingResult
{
    public List<BingResourceSet> ResourceSets { get; set; }
}

public class BingResourceSet
{
    public List<BingResource> Resources { get; set; }
}

public class BingResource
{
    public string Name { get; set; }
    public string Confidence { get; set; }
    public string EntityType { get; set; }
    public List<BingPoint> GeocodePoints { get; set; }
    public List<BingCell> Results { get; set; }
    public double[] Bbox { get; set; }
}

public class BingPoint
{
    public double[] Coordinates { get; set; }
    public string Type { get; set; }
    public List<string> UsageTypes { get; set; }
}

public class BingCell
{
    public int OriginIndex { get; set; }
    public int DestinationIndex { get; set; }
    public double TravelDistance { get; set; }
}
