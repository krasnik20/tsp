﻿namespace TSP.Server.Services.GeoServiceAdapters.Bing;

public class DistanceMatrixRequestBody
{
    public IEnumerable<BingCoords> Origins { get; set; }
    public IEnumerable<BingCoords> Destinations { get; set; }
    public string TravelMode { get; set; } = "driving";
}

public class BingCoords
{
    public double Latitude { get; set; }
    public double Longitude { get; set; }
}
