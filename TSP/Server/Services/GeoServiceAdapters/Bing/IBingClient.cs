﻿using Refit;

namespace TSP.Server.Services.GeoServiceAdapters.Bing;

public interface IBingClient
{
    [Post("/Routes/DistanceMatrix")]
    Task<BingResult> GetDistanceMatrix(string key, [Body(buffered: true)]  DistanceMatrixRequestBody request, CancellationToken ct = default);

    [Get("/Locations/{query}")]
    Task<BingResult> GetAddresses(string query, string key, int maxResults = 10, string c = "ru", string ur = "RU", CancellationToken ct = default);
}
