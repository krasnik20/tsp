﻿namespace TSP.Server.Services.Solvers;

public interface ICommisVoyagerSolver
{
    Task<SolutionResult> Solve(double[,] distances, CancellationToken ct = default);
}
