﻿namespace TSP.Server.Services.Solvers;

public abstract class CommisVoyagerSolverBase : ICommisVoyagerSolver
{
    public async Task<SolutionResult> Solve(double[,] distances, CancellationToken ct = default)
    {
        ValidateInput(distances);

        return await SolveInternal(distances, ct);
    }

    private static void ValidateInput(double[,] distances)
    {
        var N = distances.GetLength(0);
        if (N < 3 || N != distances.GetLength(1))
            throw new ArgumentException();
    }

    protected static SolutionResult GetResultFromPath(List<int> path, double[,] distances)
    {
        double distance = 0;
        for (int i = 1; i < path.Count; i++)
        {
            distance += distances[path[i - 1], path[i]];
        }
        return new SolutionResult(distance, path);
    }

    protected abstract Task<SolutionResult> SolveInternal(double[,] distances, CancellationToken ct = default);

    protected static List<int> GetPathFromParts(List<PathPart> parts)
    {
        List<int> path = new() { 0 };
        while (parts.Count > 0)
        {
            var last = path.Last();
            var part = parts.FirstOrDefault(p => p.FirstCity == last);
            path.Add(part.SecondCity);
            parts.Remove(part);
        }

        return path;
    }
}

public class PathPart
{
    public int FirstCity;
    public int SecondCity;

    public PathPart(int firstCity, int secondCity)
    {
        FirstCity = firstCity;
        SecondCity = secondCity;
    }
}
