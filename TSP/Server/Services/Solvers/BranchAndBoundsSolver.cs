﻿namespace TSP.Server.Services.Solvers;

public class BranchAndBoundsSolver : CommisVoyagerSolverBase
{
    protected override Task<SolutionResult> SolveInternal(double[,] distances, CancellationToken ct = default)
    {
        var N = distances.GetLength(0);

        for (int i = 0; i < N; i++)
            distances[i, i] = double.PositiveInfinity;

        var (reduced, h) = ReduceRowsAndCols(distances);

        var root = new Node() 
        { 
            Distances = reduced, 
            Value = h 
        };

        var curNode = root;

        while (curNode.Level != N)
        {
            if (ct.IsCancellationRequested) return Task.FromResult(new SolutionResult(double.MaxValue, null));
            Branch(curNode);
            curNode = FindBestNode(root);
        }

        var distance = curNode.Value;

        var path = GetPathFromParts(GetPathParts(curNode));

        return Task.FromResult(new SolutionResult(distance, path));
    }

    private static List<PathPart> GetPathParts(Node curNode)
    {
        List<PathPart> parts = new();
        while (curNode != null)
        {
            if (curNode.IsIncluding) parts.Add(curNode.PathPart);
            curNode = curNode.Parent;
        }

        return parts;
    }

    private static void Branch(Node node)
    {
        var (maxFine, maxFineI, maxFineJ) = FindMaxFine(node.Distances);

        node.SetSiblings(
            MarkPathPartAsIncluded(node.Distances, maxFineI, maxFineJ, node.Value), 
            MarkPathPartAsCanceled(node.Distances, maxFineI, maxFineJ, node.Value, maxFine)
        );

        var parts = GetPathParts(node);

        bool includeIsCorrect = true;
        int first = node.Including.PathPart.FirstCity;
        int last = node.Including.PathPart.SecondCity;
        while (true)
        {
            if (first == last && parts.Count != 0) //если круг замкнулся, но не все его части были использованы
            {
                includeIsCorrect = false;
                break;
            }
            var part = parts.FirstOrDefault(p => p.FirstCity == last);
            if (part == null) break;
            last = part.SecondCity;
            parts.Remove(part);
        }

        if (!includeIsCorrect)
            node.Including.Value = double.PositiveInfinity;
    }

    private static Node FindBestNode(Node root)
    {
        if (!root.Branched)
            return root;
        
        var bestIncluding = FindBestNode(root.Including);
        var bestExcluding = FindBestNode(root.Excluding);
        var res = bestExcluding.Value > bestIncluding.Value ? bestIncluding : bestExcluding;
        return res;
    }

    private static (double MaxFine, int maxFineI, int maxFineJ) FindMaxFine(double[,] matrix)
    {
        int N = matrix.GetLength(0);
        double maxFine = double.MinValue;
        int maxFineI = 0, maxFineJ = 0;
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (matrix[i, j] == 0d)
                {
                    var minInRow = double.PositiveInfinity;
                    var minInCol = double.PositiveInfinity;
                    for (int k = 0; k < N; k++)
                    {
                        if (k != j && minInRow > matrix[i, k])
                            minInRow = matrix[i, k];
                        if (k != i && minInCol > matrix[k, j])
                            minInCol = matrix[k, j];
                    }
                    var fine = minInRow + minInCol;
                    if (fine > maxFine) 
                        (maxFine, maxFineI, maxFineJ) = (fine, i, j);
                }
        return (maxFine, maxFineI, maxFineJ);
    }

    private static (double[,] reduced, double h) ReduceRowsAndCols(double[,] matrix, double h = 0, bool needsCopy = true)
    {
        var N = matrix.GetLength(0);
        var copy = needsCopy ? new double[N, N] : matrix;
        if (needsCopy) Array.Copy(matrix, copy, matrix.Length);
        var rowsMins = new double[N];
        var colsMins = new double[N];
        for (int i = 0; i < N; i++)
            rowsMins[i] = colsMins[i] = double.PositiveInfinity;

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (copy[i, j] < rowsMins[i])
                    rowsMins[i] = copy[i, j];

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (double.IsFinite(copy[i, j]))
                    copy[i, j] -= rowsMins[i];

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (copy[i, j] < colsMins[j])
                    colsMins[j] = copy[i, j];

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (double.IsFinite(copy[i, j]))
                    copy[i, j] -= colsMins[j];

        return (copy, 
                h += colsMins.Where(double.IsFinite).Sum() 
                   + rowsMins.Where(double.IsFinite).Sum());
    }
    
    private static Node MarkPathPartAsCanceled(double[,] matrix, int i, int j, double h, double fine)
    {
        var N = matrix.GetLength(0);
        var copy = new double[N, N];
        Array.Copy(matrix, copy, matrix.Length);
        copy[i, j] = double.PositiveInfinity;
        var (reduced, _) = ReduceRowsAndCols(copy, h, false);

        return new Node { Distances = reduced, Value = h + fine, PathPart = new PathPart(i, j), IsIncluding = false };
    }

    private static Node MarkPathPartAsIncluded(double[,] matrix, int maxFineI, int maxFineJ, double h)
    {
        var N = matrix.GetLength(0);
        var copy = new double[N, N];
        Array.Copy(matrix, copy, matrix.Length);

        for (int i = 0; i < N; i++)
            copy[i, maxFineJ] = copy[maxFineI, i] = double.PositiveInfinity;
        copy[maxFineJ, maxFineI] = double.PositiveInfinity;

        var (reduced, newh) = ReduceRowsAndCols(copy, h, false);

        return new Node { Distances = reduced, Value = newh, PathPart = new PathPart(maxFineI, maxFineJ), IsIncluding = true };
    }

    private class Node
    {
        public double Value { get; set; }
        public double[,] Distances { get; set; }
        public PathPart PathPart { get; set; }
        public bool Branched => Excluding != null && Including != null;
        public bool IsIncluding { get; set; }
        public int Level { get; set; }
        public Node Excluding { get; private set; }
        public Node Including { get; private set; }
        public Node Parent { get; private set; }

        public void SetSiblings(Node Including, Node Excluding)
        {
            this.Including = Including;
            this.Excluding = Excluding;
            Including.Parent = this;
            Excluding.Parent = this;
            Including.Level = Level + 1;
            Excluding.Level = Level;
        }
    }
}
