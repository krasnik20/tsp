﻿namespace TSP.Server.Services.Solvers;

public class ExhaustiveSearchSolver : CommisVoyagerSolverBase
{
    private const int _maxCitiesCount = 11;

    protected override Task<SolutionResult> SolveInternal(double[,] distances, CancellationToken ct = default)
    {
        var N = distances.GetLength(0);
        if (N > _maxCitiesCount)
        {
            return Task.FromResult(new SolutionResult(double.MaxValue, null));
            //throw new ArgumentOutOfRangeException();
        }

        return Task.FromResult(ExhaustiveSearchRecursive(N + 1, N, new List<int> { 0, 0 }, distances));            
    }

    private SolutionResult ExhaustiveSearchRecursive(
        int targetPathLength, 
        int citiesCount, 
        List<int> path, 
        double[,] distances, 
        CancellationToken ct = default)
    {
        if (ct.IsCancellationRequested) return new SolutionResult(double.MaxValue, null);

        if (path.Count == targetPathLength)
            return GetResultFromPath(path, distances);

        var bestResult = new SolutionResult(double.MaxValue, null);
        for (int i = 0; i < citiesCount; i++)
        {
            if (path.Contains(i)) continue;

            path.Insert(path.Count - 1, i);

            var result = ExhaustiveSearchRecursive(targetPathLength, citiesCount, path, distances);

            if (result.Distance < bestResult.Distance) 
                bestResult = new SolutionResult(result.Distance, new List<int>(result.Path));

            path.Remove(i);
        }
        return bestResult;
    }
}
