﻿namespace TSP.Server.Services.Solvers;

public class GeneticSolver : CommisVoyagerSolverBase
{
    public int Generations { get; set; }
    public int Population { get; set; }
    public double MutationPossibility { get; set; }

    private static int _groupSize = 5;
    private static double _closeCitiesPossibility = 0.75;
    private static int _numberOfCloseCities = 3;

    protected override Task<SolutionResult> SolveInternal(double[,] distances, CancellationToken ct = default)
    {
        int citiesCount = distances.GetLength(0);
        List<SolutionResult> curPopulation = GeneratePopulation(distances, Population);

        for (int generation = 0; generation < Generations; generation++)
        {
            var parents = curPopulation.OrderRandomly().Take(_groupSize).OrderBy(c => c.Distance).Take(2);

            var (firstChild, secondChild) = Crossover(parents.First(), parents.Last());

            curPopulation.Add(GetResultFromPath(firstChild.Mutate(MutationPossibility), distances));
            curPopulation.Add(GetResultFromPath(secondChild.Mutate(MutationPossibility), distances));
            curPopulation = curPopulation.OrderBy(p => p.Distance).Take(Population).ToList();
        }

        return Task.FromResult(curPopulation.First());
    }

    private static List<SolutionResult> GeneratePopulation(double[,] distances, int populationCount)
    {
        var random = new Random();
        var maxIndex = distances.GetLength(0) - 1;
        var result = new List<SolutionResult>();

        for (int pathCount = 0; pathCount < populationCount; pathCount++)
        {
            var firstCity = random.Next(maxIndex);
            var lastCity = firstCity;
            var pathParts = new List<PathPart>();

            for (int city = 0; city < maxIndex; city++)
            {
                int nextCity = -1;
                var closeCities = distances
                    .GetRow(city)
                    .Select((d, i) => (distance: d, index: i))
                    .OrderBy(c => c.distance)
                    .Select(c => c.index)
                    .Take(_numberOfCloseCities)
                    .ToArray();

                int attempts = 0;
                do
                {
                    nextCity = random.NextDouble() < _closeCitiesPossibility && attempts <= _numberOfCloseCities 
                        ? closeCities[random.Next(_numberOfCloseCities)] 
                        : random.Next(maxIndex + 1);
                    attempts++;
                }
                while (pathParts.Any(p => p.FirstCity == nextCity) || (nextCity == lastCity));

                pathParts.Add(new PathPart(lastCity, nextCity));
                lastCity = nextCity;
            }

            pathParts.Add(new PathPart(lastCity, firstCity));

            result.Add(GetResultFromPath(GetPathFromParts(pathParts), distances));
        }

        return result;
    }

    private (List<int> first, List<int> second) Crossover(SolutionResult firstParent, SolutionResult secondParent)
    {
        var cut = new Random().Next(1, firstParent.Path.Count - 1);
        return
        (
            CombineParents(firstParent, secondParent, cut),
            CombineParents(secondParent, firstParent, cut)
        );
    }

    private List<int> CombineParents(SolutionResult firstParent, SolutionResult secondParent, int cut)
    {
        var child = new List<int>();

        for (int i = 0; i < cut; i++)
            child.Add(firstParent.Path[i]);

        for (int i = cut; i < firstParent.Path.Count - 1; i++)
            child.Add(child.Contains(secondParent.Path[i])
                ? firstParent.Path.First(p => !child.Contains(p))
                : secondParent.Path[i]);

        child.Add(0);

        return child;
    }
}

static class Extensions
{
    private static Random random;

    static Extensions() => random = new Random();

    public static IEnumerable<T> OrderRandomly<T>(this IEnumerable<T> source)
    {
        return source.OrderBy(x => random.Next());
    }

    public static List<T> Mutate<T>(this List<T> source, double possibility)
    {
        var r = new Random();
        if (r.NextDouble() > possibility) return source;

        int firstGene = GenerateGene(), secondGene = GenerateGene();

        while (firstGene == secondGene) secondGene = GenerateGene();

        (source[firstGene], source[secondGene]) = (source[secondGene], source[firstGene]);

        return source;

        int GenerateGene() => r.Next(1, source.Count - 1);
    }

    public static IEnumerable<T> GetRow<T>(this T[,] matrix, int rowNumber)
    {
        return Enumerable
            .Range(0, matrix.GetLength(1))
            .Select(x => matrix[rowNumber, x]);
    }
}
