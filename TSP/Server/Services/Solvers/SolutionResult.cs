﻿namespace TSP.Server.Services.Solvers;

public record SolutionResult(double Distance, List<int> Path);
