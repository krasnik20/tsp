﻿using Mediator;
using TSP.Server.Services.GeoServiceAdapters;
using TSP.Shared.Exceptions;

namespace TSP.Server.CQRS.Queries;

public record GetDistancesQuery(ICollection<Address> Addresses) : IRequest<double[,]>;

public class GetDistancesQueryHandler : IRequestHandler<GetDistancesQuery, double[,]>
{
    private readonly IGeoServiceAdapter _geoServiceAdapter;

    public GetDistancesQueryHandler(IGeoServiceAdapter geoServiceAdapter)
    {
        _geoServiceAdapter = geoServiceAdapter;
    }

    public async ValueTask<double[,]> Handle(GetDistancesQuery request, CancellationToken ct)
    {
        var distances = await _geoServiceAdapter.GetDistances(
            request.Addresses,
            ct);

        ValidateDistances(distances);

        return distances;
    }

    private static void ValidateDistances(double[,] distances)
    {
        foreach (var d in distances)
            if (d < 0) throw new UnreachablePointsException();
    }
}
