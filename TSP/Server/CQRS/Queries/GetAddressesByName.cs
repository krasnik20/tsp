﻿using Mediator;
using TSP.Server.Services.GeoServiceAdapters;

namespace TSP.Server.CQRS.Queries;

public sealed record GetAddressesByNameQuery(string SearchKey) : IRequest<ICollection<Address>>;

public sealed class GetAddressesByNameQueryHandler : IRequestHandler<GetAddressesByNameQuery, ICollection<Address>>
{
    private readonly IGeoServiceAdapter _geoServiceAdapter;

    public GetAddressesByNameQueryHandler(IGeoServiceAdapter geoServiceAdapter)
    {
        _geoServiceAdapter = geoServiceAdapter;
    }

    public async ValueTask<ICollection<Address>> Handle(GetAddressesByNameQuery request, CancellationToken ct)
    {
        return await _geoServiceAdapter.SearchAddresses(
            request.SearchKey,
            ct);
    }
}
