﻿using Mediator;
using Microsoft.Extensions.Options;
using TSP.Server.Options;
using TSP.Server.Services.Solvers;

namespace TSP.Server.CQRS.Queries
{
    public record GetTSPSolutionQuery(double[,] Distances) : IRequest<SolutionResult>;

    public class GetTSPSolutionQueryHandler : IRequestHandler<GetTSPSolutionQuery, SolutionResult>
    {
        private readonly GeneticSolverOptions _options;

        public GetTSPSolutionQueryHandler(IOptions<GeneticSolverOptions> options)
        {
            _options = options.Value;
        }

        public async ValueTask<SolutionResult> Handle(GetTSPSolutionQuery request, CancellationToken ct)
        {
            var solver = GetSolver(request.Distances.GetLength(0));

            return await solver.Solve(request.Distances, ct);
        }

        private ICommisVoyagerSolver GetSolver(int addressesCount)
        {
            if (addressesCount > 11)
            {
                return new GeneticSolver
                {
                    Generations = _options.Generations,
                    MutationPossibility = _options.MutationPossibility,
                    Population = _options.Population
                };
            }
            else if (addressesCount > 8)
            {
                return new ExhaustiveSearchSolver();
            }
            else
            {
                return new BranchAndBoundsSolver();
            }
        }
    }
}
