﻿using Mediator;

namespace TSP.Server.CQRS.Queries;

public sealed record GetRouteQuery(ICollection<Address> Addresses) : IRequest<RouteResult>;

public sealed class GetRouteRequestHandler : IRequestHandler<GetRouteQuery, RouteResult>
{
    private readonly IMediator _mediator;

    public GetRouteRequestHandler(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async ValueTask<RouteResult> Handle(GetRouteQuery request, CancellationToken ct)
    {
        var distances = await _mediator.Send(new GetDistancesQuery(request.Addresses), ct);

        var solution = await _mediator.Send(new GetTSPSolutionQuery(distances), ct);

        return new RouteResult
        {
            Length = solution.Distance,
            Addresses = solution.Path
                .Join(
                    request.Addresses.Select((address, i) => (address, i)), 
                    i => i, 
                    address => address.i, 
                    (i, a) => a.address)
                .ToList()
        };
    }
}
