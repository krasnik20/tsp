﻿using TSP.Shared.Models;

namespace TSP.Shared;

public static class Utils
{
    private const double eps = 0.0005;

    public static bool CheckAddressIsUnique(Address address, IEnumerable<Address> addresses) =>
        !addresses.Any(s => Math.Abs(s.Longitude - address.Longitude) < eps && Math.Abs(s.Latitude - address.Latitude) < eps);

    public static bool CheckAddressesDoubled(IEnumerable<Address> addresses) => 
        addresses.Any(s => CheckAddressIsUnique(s, addresses));
}
