﻿namespace TSP.Shared.Models;

public class RouteResult
{
    public List<Address> Addresses { get; set; }
    public double Length { get; set; }
}
