﻿namespace TSP.Shared.Models;

public class Address
{
    public double Longitude { get; set; }
    public double Latitude { get; set; }
    public string Name { get; set; }
    public double[] Bounds { get; set; }
}