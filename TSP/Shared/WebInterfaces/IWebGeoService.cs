﻿using Refit;
using TSP.Shared.Models;

namespace TSP.Shared.WebInterfaces;

public interface IWebGeoService
{
    [Get("/Addresses/SearchAddresses")]
    public Task<ICollection<Address>> SearchAddresses(string searchKey);
    [Post("/Addresses/FindRoute")]
    public Task<RouteResult> FindRoute(ICollection<Address> addresses);
}
